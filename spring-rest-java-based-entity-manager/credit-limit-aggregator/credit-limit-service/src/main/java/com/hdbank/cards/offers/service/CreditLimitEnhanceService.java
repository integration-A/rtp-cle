package com.hdbank.cards.offers.service;

import com.hdbank.cards.offers.service.beans.CreditLimitEnhanceServResp;
import com.hdbank.offers.dao.CreditLimitDao;
import com.hdbank.offers.dao.beans.CreditLimitEnhanceDaoResp;
import com.hdbank.offers.exception.BusnessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

public interface CreditLimitEnhanceService {

	public CreditLimitEnhanceServResp verifyPromocode(String promocode) throws BusnessException;

}

	/**
	 * @author AnilAleti
	 *
	 *         Sep 5, 2019
	 */


