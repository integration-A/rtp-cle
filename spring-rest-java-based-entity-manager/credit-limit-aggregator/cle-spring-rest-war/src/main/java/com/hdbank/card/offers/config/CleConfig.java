package com.hdbank.card.offers.config;

import com.hdbank.offers.dao.config.CLEDaoConfig;
import com.hdbank.offers.dao.config.CLEPersistenceJNDIConfig;
import org.springframework.context.annotation.*;
import com.hdbank.cards.offers.config.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan({"com.hdbank.card.offers.*"})
@Import({CLEServiceConfig.class, CLEPersistenceJNDIConfig.class})
@EnableWebMvc
public class CleConfig {
}
