package com.sib.cards.credit.offers.api.config;

import com.google.common.io.Resources;

import java.io.InputStream;
import java.util.Properties;


public class ApplicationPropertiesConfig {
	static Properties properties;
	static {
		properties = new Properties();

	}

	public static  Properties getEnvironmentProperties() {
		if (properties == null) {
			properties = new Properties();
		}

		try {
			String env = System.getProperty("env");
			//env can be test or dev or uat or prod
			//remote tomcat 
			//env=test
			System.out.println(env);
			InputStream props = Resources.getResource(env + ".properties").openStream();
			properties.load(props);

		} catch (Exception e) {

		}

		return properties;
	}

}
