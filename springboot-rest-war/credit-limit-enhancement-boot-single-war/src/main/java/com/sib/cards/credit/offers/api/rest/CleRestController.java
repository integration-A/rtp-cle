package com.sib.cards.credit.offers.api.rest;



import com.sib.cards.credit.offers.api.beans.CreditLimitEnhanceResponse;
import com.sib.cards.credit.offers.api.beans.CreditLimitEnhanceServResp;
import com.sib.cards.credit.offers.api.config.ApplicationPropertiesConfig;
import com.sib.cards.credit.offers.api.exception.BusnessException;
import com.sib.cards.credit.offers.api.service.CreditLimitEnhanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cle")
public class CleRestController {

    @Autowired
    private CreditLimitEnhanceService creditLimitEnhanceService;

    @RequestMapping(value = "/health", method = RequestMethod.GET, produces = {MediaType.TEXT_HTML_VALUE})
    public String healthCheck() {

        return "service is running fine.....";

    }

    @RequestMapping(value = "/verifypromocode", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public ResponseEntity<CreditLimitEnhanceResponse> verifyPromocode(@RequestParam("promocode") String promocode) throws BusnessException {
        System.out.println(creditLimitEnhanceService.verifyPromocode(promocode));
        System.out.println("country code......." + ApplicationPropertiesConfig.getEnvironmentProperties().getProperty("countryCode"));
       CreditLimitEnhanceServResp creditLimitEnhanceServResp= creditLimitEnhanceService.verifyPromocode(promocode);
        CreditLimitEnhanceResponse creditLimitEnhanceResponse = new CreditLimitEnhanceResponse();

        creditLimitEnhanceResponse.setCurrentLimit(creditLimitEnhanceServResp.getCurrentLimit());
        creditLimitEnhanceResponse.setEligbleAmount(creditLimitEnhanceServResp.getEligbleAmount());
        creditLimitEnhanceResponse.setExpiryDate(creditLimitEnhanceServResp.getExpiryDate());
        creditLimitEnhanceResponse.setPromocode(creditLimitEnhanceServResp.getPromocode());

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(creditLimitEnhanceResponse);
    }


}
