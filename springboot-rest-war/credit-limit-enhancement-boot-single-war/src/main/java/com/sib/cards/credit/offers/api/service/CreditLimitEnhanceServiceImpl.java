

package com.sib.cards.credit.offers.api.service;


import com.sib.cards.credit.offers.api.beans.CreditLimitEnhanceServResp;
import com.sib.cards.credit.offers.api.entity.CleOfferInfo;
import com.sib.cards.credit.offers.api.exception.BusnessException;

import com.sib.cards.credit.offers.api.repository.CleRepositoryI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service

@Transactional(value="transactionManager", readOnly = true)
public class CreditLimitEnhanceServiceImpl implements CreditLimitEnhanceService {
   @Autowired
    private CleRepositoryI cleRepository;
    @Override
    public CreditLimitEnhanceServResp verifyPromocode(String promocode) throws BusnessException {
       CleOfferInfo cleOfferInfo = cleRepository.verifyPromocode(promocode);
        CreditLimitEnhanceServResp creditLimitEnhanceServResp =  new CreditLimitEnhanceServResp();
        creditLimitEnhanceServResp.setCurrentLimit(cleOfferInfo.getCurrentLimit());
        creditLimitEnhanceServResp.setEligbleAmount(cleOfferInfo.getEligbleAmount());
        creditLimitEnhanceServResp.setExpiryDate(cleOfferInfo.getExpiryDate());
        creditLimitEnhanceServResp.setPromocode(cleOfferInfo.getPromocode());

        return creditLimitEnhanceServResp;
    }


}

