package com.sib.cards.credit.offers.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "cleofferinfo")
@Entity
public class CleOfferInfo {
	@Id
	@Column(name = "promocode")
	private String promocode;
	@Column(name = "cardNumber")
	private String cardNumber;
	@Column(name = "customerId")
	private String customerId;


	@Column(name = "currentLimit")
	private Double currentLimit;
	@Column(name = "eligbleAmount")
	private Double eligbleAmount;
	@Column(name = "expiryDate")
	private String expiryDate;
	@Override
	public String toString() {
		return "CleOfferInfo [cardNumber=" + cardNumber + ", customerId=" + customerId + ", promocode=" + promocode
				+ ", currentLimit=" + currentLimit + ", eligbleAmount=" + eligbleAmount + ", expiryDate=" + expiryDate
				+ "]";
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getPromocode() {
		return promocode;
	}
	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}
	public Double getCurrentLimit() {
		return currentLimit;
	}
	public void setCurrentLimit(Double currentLimit) {
		this.currentLimit = currentLimit;
	}
	public Double getEligbleAmount() {
		return eligbleAmount;
	}
	public void setEligbleAmount(Double eligbleAmount) {
		this.eligbleAmount = eligbleAmount;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	

}
