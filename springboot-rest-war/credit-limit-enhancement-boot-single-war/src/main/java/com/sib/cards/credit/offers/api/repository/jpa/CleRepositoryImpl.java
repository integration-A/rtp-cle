package com.sib.cards.credit.offers.api.repository.jpa;


import com.sib.cards.credit.offers.api.entity.CleOfferInfo;

import com.sib.cards.credit.offers.api.repository.CleRepositoryI;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class CleRepositoryImpl implements CleRepositoryI {

	/*@Autowired
	private SessionFactory sessionFactory;*/
	@PersistenceContext
	private EntityManager entityManager;

	public CleOfferInfo verifyPromocode(String promocode)  {


		CleOfferInfo cleOfferInfo=(CleOfferInfo) entityManager.find(CleOfferInfo.class,promocode);



		return cleOfferInfo;
	}


}
