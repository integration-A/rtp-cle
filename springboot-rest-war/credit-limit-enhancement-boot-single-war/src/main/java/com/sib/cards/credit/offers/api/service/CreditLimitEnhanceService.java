package com.sib.cards.credit.offers.api.service;


import com.sib.cards.credit.offers.api.beans.CreditLimitEnhanceServResp;
import com.sib.cards.credit.offers.api.exception.BusnessException;

public interface CreditLimitEnhanceService {

	public CreditLimitEnhanceServResp verifyPromocode(String promocode) throws BusnessException;

}

	/**
	 * @author AnilAleti
	 *
	 *         Sep 5, 2019
	 */


