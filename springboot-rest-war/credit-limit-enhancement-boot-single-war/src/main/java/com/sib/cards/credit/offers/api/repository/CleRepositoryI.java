package com.sib.cards.credit.offers.api.repository;


import com.sib.cards.credit.offers.api.entity.CleOfferInfo;
import com.sib.cards.credit.offers.api.exception.BusnessException;

public interface CleRepositoryI {

	public CleOfferInfo verifyPromocode(String promocode) throws BusnessException;

}
