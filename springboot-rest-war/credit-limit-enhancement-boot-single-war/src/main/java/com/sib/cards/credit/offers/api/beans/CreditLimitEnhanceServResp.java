package com.sib.cards.credit.offers.api.beans;

public class CreditLimitEnhanceServResp {

	private String promocode;
	private Double currentLimit;
	private Double eligbleAmount;
	private String expiryDate;

	public String getPromocode() {
		return promocode;
	}

	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

	public Double getCurrentLimit() {
		return currentLimit;
	}

	public void setCurrentLimit(Double currentLimit) {
		this.currentLimit = currentLimit;
	}

	public Double getEligbleAmount() {
		return eligbleAmount;
	}

	public void setEligbleAmount(Double eligbleAmount) {
		this.eligbleAmount = eligbleAmount;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@Override
	public String toString() {
		return "CreditLimitEnhanceServResp [promocode=" + promocode + ", currentLimit=" + currentLimit
				+ ", eligbleAmount=" + eligbleAmount + ", expiryDate=" + expiryDate + "]";
	}

}
