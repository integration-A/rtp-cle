package com.sib.cards.credit.offers.api.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CCLEExceptionAdvicer {

	@ExceptionHandler(Throwable.class)
	public ResponseEntity<CleErrorResponse> handleGlobalException(Throwable exception) {
		
		System.out.println("enterd into handle global exception");

		if (exception instanceof BusnessException) {
			System.out.println("enterd into  business  exception if block{}");
			BusnessException businessException = (BusnessException) exception;

			CleErrorResponse CleErrorResponse = prepareCleErrorResponse(businessException.getErrorCode(),
					businessException.getErrorMessage());

			return ResponseEntity.status(HttpStatus.CONFLICT).body(CleErrorResponse);
		}

		return null;

	}

	private CleErrorResponse prepareCleErrorResponse(String errorCode, String errorMessage) {

		CleErrorResponse CleErrorResponse = new CleErrorResponse();
		CleErrorResponse.setErrorCode(errorCode);
		CleErrorResponse.setErrorMessage(errorMessage);

		return CleErrorResponse;

	}

}
