package com.hdbank.cards.offers.resouce;

import static org.junit.Assert.*;

import javax.ws.rs.core.Response;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.hdbank.cards.offers.resouce.response.CreditLimitEnhanceResponse;
import com.hdbank.cards.offers.service.CreditLimitEnhanceService;
import com.hdbank.cards.offers.service.CreditLimitEnhanceServiceImpl;
import com.hdbank.cards.offers.service.beans.CreditLimitEnhanceServResp;
import com.hdbank.offers.dao.impl.CreditLimitDaoImpl;

import junit.framework.Assert;
	@RunWith(PowerMockRunner.class)
	@PrepareForTest(CreditLimitEnhanceResource.class)
	public class CreditLimitEnhanceResourceTest {
		private CreditLimitEnhanceResource creditLimitEnhanceResource;
		private CreditLimitEnhanceResponse creditLimitEnhanceResponse;
		CreditLimitEnhanceServResp creditLimitEnhanceServResp=new CreditLimitEnhanceServResp();

	    private CreditLimitEnhanceServiceImpl creditLimitEnhanceServiceimplMock;

	@Before
	public void setUp() throws Exception {
		creditLimitEnhanceResource=new CreditLimitEnhanceResource();
		creditLimitEnhanceServiceimplMock=PowerMockito.mock(CreditLimitEnhanceServiceImpl.class);
	}

	@Test
	public void testVerifyPromocode() throws Exception {
		PowerMockito.whenNew(CreditLimitEnhanceServiceImpl.class).withAnyArguments().thenReturn(creditLimitEnhanceServiceimplMock);
		PowerMockito.when(creditLimitEnhanceServiceimplMock.verifyPromocode(Matchers.anyString())).thenReturn(prepareResponse());
	    Response response=creditLimitEnhanceResource.verifyPromocode("hdbank100");
		//Response response= Response.ok().status(200).entity(creditLimitEnhanceResource.verifyPromocode("hdbank100")).build();
	    CreditLimitEnhanceResponse crEnhanceResponse =(CreditLimitEnhanceResponse)response.getEntity();
		 Assert.assertEquals(new Double(2000),crEnhanceResponse.getCurrentLimit());
		 System.out.print("enhanceResp"+creditLimitEnhanceResponse);
			System.out.print("resp"+response);
	}

	private CreditLimitEnhanceServResp prepareResponse() {
		CreditLimitEnhanceServResp creditLimitEnhanceServResp=new CreditLimitEnhanceServResp();
		creditLimitEnhanceServResp.setCurrentLimit(2000d);
		creditLimitEnhanceServResp.setEligbleAmount(1000d);
		creditLimitEnhanceServResp.setExpiryDate("1221122");
		creditLimitEnhanceServResp.setPromocode("hdbank100");
		return creditLimitEnhanceServResp;
	}

}
