package com.hdbank.offers.exception;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hdbank.cards.offers.utility.CleValidator;

import junit.framework.Assert;

public class CleValidatorTest {

	private CleValidator cleValidator = null;

	@Before
	public void setup() {
		cleValidator = new CleValidator();

	}

	@Test
	public void testValidateWithNull() {

		String expected = "cleErrorcode201";
		try {

			cleValidator.validate(null);

		} catch (BusnessException e) {

			Assert.assertEquals(expected, e.getErrorCode());

		}

	}
	
	@Test
	public void testValidateWithEmpty() {

		String expected = "cleErrorcode201";
		try {

			cleValidator.validate("");

		} catch (BusnessException e) {

			Assert.assertEquals(expected, e.getErrorCode());

			e.printStackTrace();
		}

	}

	@After

	public void tearDown() {
		cleValidator = null;

	}

}
