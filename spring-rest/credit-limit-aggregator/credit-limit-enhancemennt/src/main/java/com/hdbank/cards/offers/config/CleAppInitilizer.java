package com.hdbank.cards.offers.config;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.hdbank.cards.offers.resouce.CreditLimitEnhanceResource;
import com.hdbank.offers.exception.CleExceptionMapper;

@ApplicationPath("/api")
public class CleAppInitilizer extends Application {

	@Override
	public Set<Class<?>> getClasses() {

		Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(CreditLimitEnhanceResource.class);

		classes.add(CleExceptionMapper.class);

		return classes;
	}

}
