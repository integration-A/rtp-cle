package com.hdbank.cards.offers.resouce;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;

import com.hdbank.cards.offers.resouce.response.CreditLimitEnhanceResponse;
import com.hdbank.cards.offers.service.CreditLimitEnhanceService;
import com.hdbank.cards.offers.service.CreditLimitEnhanceServiceImpl;
import com.hdbank.cards.offers.service.beans.CreditLimitEnhanceServResp;
import com.hdbank.cards.offers.utility.CleValidator;
import com.hdbank.offers.exception.BusnessException;
import com.hdbank.offers.exception.CleErrorResponse;

/**
 * @author AnilAleti
 *
 *         Sep 5, 2019
 */

@Path("/cle-limit")
public class CreditLimitEnhanceResource {

	private static String healthCheck = "Credit Limit Enhance Service is running fine ..";

	CreditLimitEnhanceService CreditLimitEnhanceService ;
	
	private static Logger  resourceLogger = Logger.getLogger(CreditLimitEnhanceResource.class);
	private CleValidator validator = new CleValidator();

	@GET
	@Path("/health")
	@Produces("text/html")
	public String healthCheck() {

		return healthCheck;

	}

	@Path("/promocode")
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response verifyPromocode(@QueryParam("promocode") String promocode) throws BusnessException {
	CreditLimitEnhanceService	= new CreditLimitEnhanceServiceImpl();
		resourceLogger.debug("entered into resouce layer verifyPromocode() :"+promocode);
		validator.validate(promocode);

		CreditLimitEnhanceServResp creditLimitEnhanceServResp = CreditLimitEnhanceService.verifyPromocode(promocode);
		
		resourceLogger.info("response from service layeer : "+creditLimitEnhanceServResp);
		
		CreditLimitEnhanceResponse creditLimitEnhanceResponse = new CreditLimitEnhanceResponse();

		creditLimitEnhanceResponse.setCurrentLimit(creditLimitEnhanceServResp.getCurrentLimit());
		creditLimitEnhanceResponse.setEligbleAmount(creditLimitEnhanceServResp.getEligbleAmount());
		creditLimitEnhanceResponse.setExpiryDate(creditLimitEnhanceServResp.getExpiryDate());
		creditLimitEnhanceResponse.setPromocode(creditLimitEnhanceServResp.getPromocode());
		
		resourceLogger.debug("exited from  resouce layer verifyPromocode() :"+promocode);
		return Response.ok().status(200).entity(creditLimitEnhanceResponse).build();

		// TODO: handle exception

	}

	public static void main(String[] args) throws BusnessException {

		CreditLimitEnhanceResource resouce = new CreditLimitEnhanceResource();

		resouce.verifyPromocode("100");

	}

}
