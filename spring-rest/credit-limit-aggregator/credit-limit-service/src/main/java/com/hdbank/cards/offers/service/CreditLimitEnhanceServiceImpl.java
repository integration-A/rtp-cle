package com.hdbank.cards.offers.service;

import com.hdbank.cards.offers.service.beans.CreditLimitEnhanceServResp;
import com.hdbank.offers.dao.CreditLimitDao;
import com.hdbank.offers.dao.beans.CreditLimitEnhanceDaoResp;
import com.hdbank.offers.exception.BusnessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author AnilAleti
 *
 *         Sep 5, 2019
 */
@Service
@Transactional
public class CreditLimitEnhanceServiceImpl implements CreditLimitEnhanceService {
	@Autowired
	private CreditLimitDao creditLimitDao;
	public CreditLimitEnhanceServResp verifyPromocode(String promocode) throws BusnessException {
		System.out.println("entered into service layer :");
		// it will call to dao Layer and gets the response from dao
		// Dao response{}
		// What is upcasting and downcasting?

		/*CreditLimitDao creditLimitDao = new CreditLimitDaoImpl();*/

		CreditLimitEnhanceDaoResp creditLimitEnhanceDaoResp = creditLimitDao.verifyPromocode(promocode);

		CreditLimitEnhanceServResp creditLimitEnhanceServResp = new CreditLimitEnhanceServResp();

		creditLimitEnhanceServResp.setCurrentLimit(creditLimitEnhanceDaoResp.getCurrentLimit());
		creditLimitEnhanceServResp.setEligbleAmount(creditLimitEnhanceDaoResp.getEligbleAmount());
		creditLimitEnhanceServResp.setExpiryDate(creditLimitEnhanceDaoResp.getExpiryDate());
		creditLimitEnhanceServResp.setPromocode(creditLimitEnhanceDaoResp.getPromocode());
		System.out.println("exiting  into service layer :");
		return creditLimitEnhanceServResp;
	}

}
