package com.hdbank.cards.offers.service;

import com.hdbank.cards.offers.service.beans.CreditLimitEnhanceServResp;
import com.hdbank.offers.exception.BusnessException;

public interface CreditLimitEnhanceService {

	public CreditLimitEnhanceServResp verifyPromocode(String promocode) throws BusnessException;

}
