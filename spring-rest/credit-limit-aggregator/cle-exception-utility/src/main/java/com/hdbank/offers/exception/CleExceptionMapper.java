package com.hdbank.offers.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class CleExceptionMapper implements ExceptionMapper<Throwable> {

	public Response toResponse(Throwable exception) {
		
		System.out.println("entered int excepion mapper");

		if (exception instanceof BusnessException) {

			BusnessException busnessException = (BusnessException) exception;

			CleErrorResponse cleErrorResponse = prepareErrorResponse(busnessException);

			return Response.status(409).entity(cleErrorResponse).build();

		}

		if (exception instanceof NullPointerException) {

			return null;
		}

		if (exception instanceof WebApplicationException) {

			return null;
		}
		return null;

	}

	private CleErrorResponse prepareErrorResponse(BusnessException busnessException) {

		CleErrorResponse cleErrorResponse = new CleErrorResponse();
		cleErrorResponse.setErrorCode(busnessException.getErrorCode());

		cleErrorResponse.setErrorMessage(busnessException.getErrorMessage());
		return cleErrorResponse;

	}

}
