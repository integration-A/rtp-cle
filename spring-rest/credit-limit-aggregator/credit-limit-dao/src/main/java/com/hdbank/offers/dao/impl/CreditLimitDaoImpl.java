package com.hdbank.offers.dao.impl;

import com.hdbank.offers.dao.CreditLimitDao;
import com.hdbank.offers.dao.beans.CreditLimitEnhanceDaoResp;
import com.hdbank.offers.dao.entity.CleOfferInfo;
import com.hdbank.offers.exception.BusnessException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CreditLimitDaoImpl implements CreditLimitDao {

	@Autowired
	private SessionFactory sessionFactory;
	public CreditLimitEnhanceDaoResp verifyPromocode(String promocode) throws BusnessException {
		CreditLimitEnhanceDaoResp creditLimitEnhanceDaoResp = null;
		Session session=sessionFactory.openSession();
		CleOfferInfo cleOfferInfo=(CleOfferInfo) session.load(CleOfferInfo.class,promocode);
		// //this class call to the database and gets the response
		System.out.print(cleOfferInfo);
		if (null!= cleOfferInfo) {
			creditLimitEnhanceDaoResp = new CreditLimitEnhanceDaoResp();

			creditLimitEnhanceDaoResp.setCurrentLimit(cleOfferInfo.getCurrentLimit());
			creditLimitEnhanceDaoResp.setEligbleAmount(cleOfferInfo.getEligbleAmount());
			creditLimitEnhanceDaoResp.setExpiryDate(cleOfferInfo.getExpiryDate());
			creditLimitEnhanceDaoResp.setPromocode(cleOfferInfo.getPromocode());
		}

		if (null == creditLimitEnhanceDaoResp) {

			throw new BusnessException("hdbankerrocode100", "promocode is not valid ");

		}

		return creditLimitEnhanceDaoResp;
	}
	public static void main(String args[])
	{
		CreditLimitDaoImpl creditLimitDaoImpl=new CreditLimitDaoImpl();
		try {
			creditLimitDaoImpl.verifyPromocode("hdbank100");
		} catch (BusnessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
