package com.hdbank.offers.dao;

import com.hdbank.offers.dao.beans.CreditLimitEnhanceDaoResp;
import com.hdbank.offers.exception.BusnessException;

public interface CreditLimitDao {

	public CreditLimitEnhanceDaoResp verifyPromocode(String promocode) throws BusnessException;

}
