package com.hdbank.cards.offers.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@SpringBootConfiguration
@ComponentScan("com.hdbank.cards.offers.*")


public class CLEServiceConfig {
}
