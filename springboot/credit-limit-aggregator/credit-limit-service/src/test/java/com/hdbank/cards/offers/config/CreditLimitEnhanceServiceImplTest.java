/*package com.hdbank.cards.offers.config;

import com.hdbank.cards.offers.service.CreditLimitEnhanceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.hdbank.offers.dao.beans.CreditLimitEnhanceDaoResp;
import com.hdbank.offers.dao.impl.CreditLimitDaoImpl;

import junit.framework.Assert;
@RunWith(PowerMockRunner.class)
@PrepareForTest(CreditLimitEnhanceService.CreditLimitEnhanceServiceImpl.class)
public class CreditLimitEnhanceServiceImplTest extends CreditLimitEnhanceService.CreditLimitEnhanceServiceImpl {
	private CreditLimitEnhanceServiceImpl creditLimitEnhanceServiceImpl;
	private CreditLimitDaoImpl creditLimitDaoMock;
	
	@Before
	public void setUp() throws Exception {
		creditLimitEnhanceServiceImpl=new CreditLimitEnhanceServiceImpl();
		creditLimitDaoMock=PowerMockito.mock(CreditLimitDaoImpl.class);
		
	}
	@Test
	public void testVerifyPromocode() throws Exception {
		PowerMockito.whenNew(CreditLimitDaoImpl.class).withAnyArguments().thenReturn(creditLimitDaoMock);
		PowerMockito.when(creditLimitDaoMock.verifyPromocode(Matchers.anyString())).thenReturn(prepareResponse());
		CreditLimitEnhanceServResp creditLimitEnhanceServResp=creditLimitEnhanceServiceImpl.verifyPromocode("hdbank100");
		Assert.assertEquals(new Double(5000),creditLimitEnhanceServResp.getCurrentLimit());
		System.out.print("resp"+creditLimitEnhanceServResp);
	}
	private CreditLimitEnhanceDaoResp prepareResponse() {
		CreditLimitEnhanceDaoResp creditLimitEnhanceDaoResp = new CreditLimitEnhanceDaoResp();

		creditLimitEnhanceDaoResp.setCurrentLimit(5000d);
		creditLimitEnhanceDaoResp.setEligbleAmount(6000d);
		creditLimitEnhanceDaoResp.setExpiryDate("12");
		creditLimitEnhanceDaoResp.setPromocode("hdbank100");
		return creditLimitEnhanceDaoResp;
	}

	

}
*/